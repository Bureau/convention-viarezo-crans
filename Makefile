LATEXMK ?= latexmk

SOURCE = convention-1.tex convention-2.tex
PDF = $(SOURCE:.tex=.pdf)

.PHONY: all $(PDF) clean

all: $(PDF)

$(PDF): $(SOURCE)
	$(LATEXMK) -pdflatex $(SOURCE)

clean:
	$(LATEXMK) -C $(SOURCE)
